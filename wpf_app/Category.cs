﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf_app
{
    internal class Category
    {
        public int Id { get; set; }
        public string? categoryName { get; set; }
        public List<Contact>? contacts { get; set; }

    }
}
