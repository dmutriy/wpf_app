﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace wpf_app
{
    internal class AplicationContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; } = null!;
        public DbSet<Category> Categories { get; set; } = null!;
        public AplicationContext()
        {
           Database.Migrate();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source = contacts.db");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, categoryName = "Friend" },
                new Category { Id = 2, categoryName = "Family" },
                new Category { Id = 3, categoryName = "JobContact" },
                new Category { Id = 4, categoryName = "Spam" },
                new Category { Id = 5, categoryName = "NoCategory" }
                );
        }
    }
}
