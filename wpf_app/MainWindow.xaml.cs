﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf_app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string? CategoryName;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ShowElements_Click(object sender, RoutedEventArgs e)
        {
            using (AplicationContext db = new AplicationContext())
            {
                var contacts = db.Contacts.ToList();
                MessageBox.Show($"Now in our DataBase we have \"{contacts.Count}\" elements!");
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {

            using (AplicationContext db = new AplicationContext())
            {
                var categories = db.Categories.ToList();
                foreach (var category in categories)
                {
                    if (category.categoryName == CategoryName)
                    {
                        Contact contact = new Contact
                        {
                            CategoryId = category.Id,
                            Name = Name.Text,
                            SecondName = SecondName.Text,
                            PhoneNumber = PhoneNumber.Text
                        };
                        db.Contacts.Add(contact);
                        db.SaveChanges();
                        break;
                    }
                }

                Name.Clear();
                PhoneNumber.Clear();
                SecondName.Clear();
            }
            
        }

        private void ClearLastElement_Click(object sender, RoutedEventArgs e)
        {
            using (AplicationContext db = new AplicationContext())
            {
                var contacts = db.Contacts.ToList();
                Contact? lastAdded = contacts.LastOrDefault();
                if (lastAdded != null)
                {
                    db.Contacts.Remove(lastAdded);
                    db.SaveChanges();
                    MessageBox.Show($"Contact {lastAdded.Name} {lastAdded.SecondName} has been deleted!");
                }
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton pressed = (RadioButton)sender;
            CategoryName = pressed.Content.ToString();
        }
    }
}
