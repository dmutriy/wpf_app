﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf_app
{
    internal class Contact
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? SecondName { get; set; }
        public string? PhoneNumber { get; set; }
        public Category? category { get; set; }
        public int? CategoryId { get; set; }
       
    }
}
